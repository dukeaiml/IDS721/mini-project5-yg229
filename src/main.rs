use aws_sdk_dynamodb::{Client, model::AttributeValue};
use aws_config::{load_from_env};
use lambda_runtime::{service_fn, LambdaEvent, Error};
use serde_json::Value;
use serde::Serialize;

#[tokio::main]
async fn main() -> Result<(), Error> {
    let func = service_fn(lambda_handler);
    lambda_runtime::run(func).await?;
    Ok(())
}

#[derive(Serialize)]
struct LambdaResponse {
    message: String,
}

async fn lambda_handler(_event: LambdaEvent<Value>) -> Result<LambdaResponse, Error> {
    let count = increment_and_get_count().await?;

    Ok(LambdaResponse {
        message: ("Hi, this is Yanbo's database, the total number of viewers is ").to_string() + &*count.to_string(),
    })
}

async fn increment_and_get_count() -> Result<i32, Error> {
    let config = load_from_env().await;
    let client = Client::new(&config);

    let table_name = "testdb";
    let updated_count = client.update_item()
        .table_name(table_name)
        .key("yg229", AttributeValue::S("site_access_count".to_string()))
        .update_expression("SET access_count = if_not_exists(access_count, :start) + :inc")
        .expression_attribute_values(":inc", AttributeValue::N("1".to_string()))
        .expression_attribute_values(":start", AttributeValue::N("0".to_string()))
        .return_values(aws_sdk_dynamodb::model::ReturnValue::UpdatedNew)
        .send()
        .await?;

    let count = if let Some(attributes) = updated_count.attributes {
        if let Some(attr) = attributes.get("access_count") {
            if let Ok(n) = attr.as_n() {
                n.parse::<i32>().unwrap_or(0)
            } else {
                0 // Default to 0 if not found or not a number
            }
        } else {
            0 // Default to 0 if the attribute is not present
        }
    } else {
        0 // Default to 0 if no attributes are returned
    };

    Ok(count)
}
